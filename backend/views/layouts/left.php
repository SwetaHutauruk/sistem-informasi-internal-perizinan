<aside class="main-sidebar">

    <section class="sidebar">


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Pengendalian',
                'icon' => 'th-list', 
                'url' => ['/pengendalian'],
                'items' => [
                    // ['label' => 'Prestasi',
                    // 'icon' => 'credit-card', 
                    // 'url' => ['/prestasi']],

                    // ['label' => 'Grafik Prestasi',
                    // 'icon' => 'folder-open', 
                    // 'url' => ['/highcharts']],

                    ],

                ],

                [   'label' => 'Perizinan',
                'icon'  => 'trophy', 
                'url'   => ['/perizinan'],
                'items' => [
                //  ['label' => 'Request Kompetisi',
                //  'icon' => 'credit-card', 
                //  'url' => ['/perizinan']],

                //  ['label' => 'LPJ Kompetisi',
                //  'icon' => 'file-text', 
                //  'url' => ['/lpj-kompetisi']],
             ],
         ],

         ['label' => 'Sekretariat',
         'icon' => 'calendar-o', 
         'url' => ['/sekretariat'],
        //  'items' => [
        //     ['label' => 'Request Kegiatan',
        //     'icon' => 'credit-card', 
        //     'url' => ['/kegiatan']],

        //     ['label' => 'LPJ Kegiatan',
        //     'icon' => 'file-text', 
        //     'url' => ['/lpj-kegiatan']
    ],

            // [
            //     'label' => 'Request Ruangan',
            //     'icon' => 'building-o',
            //     'url' => ['/ruangan']],
            // ],

        //     [
        //         'label' => 'Request Ruangan',
        //         'icon' => 'chevron-circle-down',
        //         'url' => ['/ruangan']],
        // ],



        ['label' => 'Penanaman Modal',
        'icon' => 'th-list', 
        'url' => ['/penanaman-modal'],
        'items' => [
            // ['label' => 'Request Beasiswa',
            // 'icon' => 'credit-card', 
            // 'url' => ['/beasiswa']],

            // ['label' => 'Informasi Beasiswa',
            // 'icon' => 'angle-double-right', 
            // 'url' => ['/beasiswa-mahasiswa']],

        ],

    ],
    // [   'label' => 'Mahasiswa Resign',
    //                 'icon'  => '    fa fa-edit', 
    //                 'url'   => ['/mhs-resign'],
    //             ],

    [   'label' => 'Manajemen User',
                    'icon'  => 'user', 
                    'url'   => ['/new-user'],
                ],

],
]) 
?>

</section>

</aside>