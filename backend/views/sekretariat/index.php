<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SekretariatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sekretariats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sekretariat-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Sekretariat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_sekretariat',
            'no_surat',
            'pengirim',
            'tanggal',
            'perihal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
