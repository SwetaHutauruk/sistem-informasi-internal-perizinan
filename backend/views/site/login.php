<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
		
body{
 background-color: #000000;


}
.container{
	
	width: 1400px ;
 height: 695px;
 text-align: center;
 margin: 0 auto;
 background-color: rgba(110, 62, 80,0.7);
 margin-top: 0px;

}

.login-background{
 font-family: arial;
 background: #35455f;
 padding: 10px;
 box-shadow: 0px 0px 15px #222;
 border-radius: 3px;
 width: 300px;
 color: #999;
 height: auto;
 margin: 50px auto;
 text-align: center;

}
input[type="text"],input[type="password"]{
  border: none;
 border-bottom: 1px solid #999;
 margin-bottom: 15px;
 color: #ffffff;
 width: 100%;
 padding: 15px 0px;
 background: none;

}


input[type="submit"]{
 background: #0eb7cc;
 color: #fff;
 border-radius: 3px;
 padding: 15px;
 width: 100%;
 border: none;
}
.img-logo{
 height: 100px;
 width: 100px;
}
a,a:hover,a:visited,a:active{
 text-decoration: none;
 color: #999;
}


</style>

<body>

<div class="container">

	<!-- <div class="row"> -->
		
		<div class="login-box">
			<div class="col-md-12 box box-radius">
				<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

     
			<div class="login-background">
     				<div class="login-header">
     					
     						<p><b>Sistem Informasi Internal Perizinan<b></p>
     
    				</div>

    			<div class="login-body">
        			<form>
        				<?= $form->field($model, 'username', ['template' => '
					<div class="col-sm-12" style="margin-top:15px;">
					<div class="input-group col-sm-12">
					<span class="input-group-addon">
					<span class="glyphicon glyphicon-user"></span>
					</span>
					{input}	
					</div>{error}{hint}
					</div>'])->textInput(['autofocus' => true])
					->input('text', ['placeholder'=>'Username']) ?>

        				<?= $form->field($model, 'password', ['template' => '
						<div class="col-sm-12" style="margin-top:15px;">
						<div class="input-group col-sm-12">
						<span class="input-group-addon">
						<span class="glyphicon glyphicon-lock"></span>
						</span>
						{input}
						</div>{error}{hint}
						</div>'])->passwordInput()
						->input('password', ['placeholder'=>'Password'])?>
        				<!-- <input type="submit" name="login" value="LOG IN"><br> -->
        				<div class="form-group">
							<input type="submit" name="login" value="LOGIN"><!-- ?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?> -->
						</div>
        				</form>
        					<p><a href="#"> Forgot Password?</a></p>

     
   				</div>
   
 
			</div>




						<?php ActiveForm::end(); ?>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

</body>




				