<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pengendalian".
 *
 * @property int $id_pengendalian
 */
class Pengendalian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pengendalian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pengendalian' => 'Id Pengendalian',
        ];
    }
}
